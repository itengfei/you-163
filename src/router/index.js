import Vue from 'vue'
import Router from 'vue-router'
import app_index from '@/components/app_index'
import cate_list from '@/components/cate_list'
import valueable_buy from '@/components/valueable_buy'
import shop_car from '@/components/shop_car'
import user from '@/components/user'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'app_index',
      component: app_index,
    },
      {
      path: '/cate_list',
      name: 'cate_list',
      component: cate_list,
    },
    {
      path: '/valueable_buy',
      name: 'valueable_buy',
      component: valueable_buy,
    },
    {
      path: '/shop_car',
      name: 'shop_car',
      component: shop_car,
    },
    {
      path: '/user',
      name: 'user',
      component: user,
    },


  ]
})
