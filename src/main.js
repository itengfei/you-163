// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import './assets/reset.css'
import './assets/rem'

import Vue from 'vue'
import App from './App'
import router from './router'

import { Button,Col, Row,Search,Tab, Tabs,Swipe,
  SwipeItem,Grid, GridItem,Tabbar, TabbarItem,Sticky,
  Sidebar, SidebarItem,Image as VanImage,
  Card,Tag,SubmitBar,AddressList,Checkbox
} from 'vant'

Vue.use(Button);
Vue.use(Col);
Vue.use(Row);
Vue.use(Search);
Vue.use(Tab);
Vue.use(Tabs);
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Grid);
Vue.use(GridItem);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Sticky);
Vue.use(Sidebar);
Vue.use(SidebarItem);
Vue.use(VanImage);
Vue.use(Card);
Vue.use(Tag);
Vue.use(SubmitBar);
Vue.use(AddressList);
Vue.use(Checkbox);

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
