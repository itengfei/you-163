var express = require('express');
var app = express();
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1')
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

// 顶部分类标签api
app.get('/get_tab_btnlist',function (req,res) {
    var tabBtnList=['推荐','居家生活','宠物生活','服饰鞋包','美食酒水',
                    '个护清洁','母婴亲子','运动旅行','数码家电','严选全球']
    res.send(tabBtnList);
})

// 轮播图api
app.get('/get_swipelsit',function (req,res) {
    var imgObj = [
            'https://yanxuan.nosdn.127.net/f3e800ea7e5a4b427c0b92ace9d93577.jpg?type=webp&imageView&quality=75&thumbnail=750x0',
            'https://yanxuan.nosdn.127.net/static-union/1642991788b8cfdd.jpg?type=webp&imageView&quality=75&thumbnail=750x0',
            'https://yanxuan.nosdn.127.net/1c31723809fc88c6b5d68f668293f7e8.jpg?type=webp&imageView&quality=75&thumbnail=750x0',
            'https://yanxuan.nosdn.127.net/1738c2636100393b5c9f00ff9a1e4107.jpg?type=webp&imageView&quality=75&thumbnail=750x0',
        ]
    res.send(imgObj);

})

var hostname='http://124.223.98.52:8080/'
app.get('/get_goodslist',function (req,res) {
    var goodsObj=[
            {
                imgurl:'../static/11.png',
                text:'新品首发'
            },
            {
                imgurl:'../static/12.png',
                text:'居家生活'
            },{
                imgurl:'../static/13.png',
                text:'服饰鞋包'
            },{
                imgurl:'../static/14.png',
                text:'美食酒水'
            },{
                imgurl:'../static/15.png',
                text:'个护清洁'
            },{
                imgurl:'../static/16.png',
                text:'母婴亲子'
            },{
                imgurl:'../static/17.png',
                text:'运动旅行'
            },{
                imgurl:'../static/18.png',
                text:'数码家电'
            },{
                imgurl:'../static/19.png',
                text:'宠物生活'
            },{
                imgurl:'../static/00.gif',
                text:'每日秒杀'
            },
        ]
    res.send(goodsObj);

})





app.listen(1234,function () {
    console.log('1234网易严选中间件,已经运行!!!!')
});